(:name "Set of Divider Ornamental Rule Line Design Elements"
 :author "WebDesignHot"
 :url "http://www.webdesignhot.com/free-vector-graphics/set-of-divider-ornamental-rule-line-design-elements/"
 :files ((:svg "webdesignhot-1-1.svg")
         (:svg "webdesignhot-1-2.svg")
         (:svg "webdesignhot-1-3.svg")
         (:svg "webdesignhot-1-4.svg")
         (:svg "webdesignhot-1-5.svg")
         (:svg "webdesignhot-1-6.svg")
         (:svg "webdesignhot-1-7.svg")
         (:svg "webdesignhot-1-8.svg")
         (:svg "webdesignhot-1-9.svg")
         (:svg "webdesignhot-1-10.svg")
         (:svg "webdesignhot-1-11.svg")
         (:svg "webdesignhot-1-12.svg")
         (:svg "webdesignhot-1-13.svg")
         (:svg "webdesignhot-1-14.svg")
         (:svg "webdesignhot-1-15.svg")
         (:svg "webdesignhot-1-16.svg")
         (:svg "webdesignhot-1-17.svg")
         (:svg "webdesignhot-1-18.svg")
         (:svg "webdesignhot-1-19.svg")))
